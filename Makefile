

default: requirements static

requirements:
	pip install -r requirements.txt

static:
	python src/manage.py collectstatic --noinput -v0 -c

