from rest_framework import serializers
from rest_framework import viewsets

from tesla.invoices.models import Invoice, Entry


class EntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Entry
        fields = ('description', 'quantity', 'price')


class InvoiceSerializer(serializers.ModelSerializer):
    total = serializers.SerializerMethodField()
    entries = EntrySerializer(many=True)

    class Meta:
        model = Invoice
        fields = (
            'ref', 'customer_name',
            'published', 'total', 'entries')

    def get_total(self, obj):
        return obj.get_total()


class InvoiceViewSet(viewsets.ModelViewSet):
    model = Invoice
    serializer_class = InvoiceSerializer

    def get_queryset(self):
        return Invoice.objects.filter(user=self.request.user)
