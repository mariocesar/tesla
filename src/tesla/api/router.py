from rest_framework import routers
from .views import InvoiceViewSet


router = routers.DefaultRouter()
router.register('invoices', InvoiceViewSet, base_name='invoice')
