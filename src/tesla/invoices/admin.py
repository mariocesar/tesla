from django.contrib import admin

from .models import Product, Invoice, Entry


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    pass


class EntryInlineAdmin(admin.TabularInline):
    model = Entry
    extra = 0
    min_num = 1


@admin.register(Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    list_display = ['ref', 'status', 'customer_name', 'user', 'published', 'created', 'get_total']
    list_filter = ['status', 'created']
    inlines = [EntryInlineAdmin]

