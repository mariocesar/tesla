from django.contrib.auth.models import User
from django.db import models
from django.db.models import F, Sum


class Product(models.Model):
    name = models.CharField(max_length=100)
    price = models.FloatField()

    def __str__(self):
        return self.name


class Invoice(models.Model):
    STATUS_CHOICES = (
        ('new', 'Nueva'),
        ('quote', 'Cotización'),
        ('paid', 'Pagada'),
        ('cancel','Cancelada')
    )

    ref = models.CharField(max_length=50)
    user = models.ForeignKey(User)
    customer_name = models.CharField(max_length=100)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='new')

    published = models.DateTimeField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.ref

    def get_total(self):
        total = (self.entries
                     .all()
                     .aggregate(
                         total=Sum(
                             F('quantity') * F('price'),
                             output_field=models.FloatField()
                         )))
        return total['total'] or 0.0

    get_total.short_description = 'Total'


class Entry(models.Model):
    invoice = models.ForeignKey(Invoice, related_name='entries')
    product = models.ForeignKey(Product, blank=True, null=True)
    description = models.TextField()
    quantity = models.IntegerField()
    price = models.FloatField()

    def __str__(self):
        return f'{self.invoice} {self.description}'

