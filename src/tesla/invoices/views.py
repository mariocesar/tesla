from django import forms
from django.views.generic import CreateView, UpdateView, ListView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.urls import reverse
from django.shortcuts import redirect
from tesla.invoices.models import Invoice


class InvoiceView(LoginRequiredMixin):
    model = Invoice

    def get_queryset(self):
        return Invoice.objects.filter(user=self.request.user)


class InvoiceList(InvoiceView, ListView):
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class InvoiceForm(forms.ModelForm):
    class Meta:
        model = Invoice
        fields = ('ref', 'customer_name', 'published')


class InvoiceCreate(InvoiceView, CreateView):
    form_class = InvoiceForm

    def form_valid(self, form):
        invoice = form.save(commit=False)
        invoice.user = self.request.user
        invoice.save()
        messages.success(self.request, 'Factura creada')
        return redirect('invoice_update', pk=invoice.id)


class InvoiceUpdate(InvoiceView, UpdateView):
    form_class = InvoiceForm

    def form_valid(self, form):
        invoice = form.save()
        messages.success(self.request, f'Factura "{invoice}" actualizada')
        return redirect('invoices_landing')


class InvoiceDelete(InvoiceView, DeleteView):
    def get_success_url(self):
        messages.warning(self.request, 'Factura eliminada')
        return reverse('invoices_landing')

