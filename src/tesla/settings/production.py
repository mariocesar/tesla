from .base import *

DEBUG = False

INSTALLED_APPS += (
    'raven.contrib.django.raven_compat',
)

RAVEN_CONFIG = {
    'dsn': 'https://cf03c205e53f45d88dd48ecc13d347a1:50839f8cc9fb4a1280fd2e285ab12777@sentry.io/185259',
}
