from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import TemplateView
from tesla.core.views import UserSettings, RegistrationView
from tesla.invoices.views import InvoiceList, InvoiceCreate, InvoiceUpdate, InvoiceDelete
from tesla.api.router import router


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^admin/docs/', include('django.contrib.admindocs.urls')),
    url(r'^accounts/$', UserSettings.as_view(), name='user_settings'),
    url(r'^accounts/register/$', RegistrationView.as_view(), name='user_registration'),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^invoices/$', InvoiceList.as_view(), name='invoices_landing'),
    url(r'^invoices/create/$', InvoiceCreate.as_view(), name='invoice_create'),
    url(r'^invoices/(?P<pk>\d+)/$', InvoiceUpdate.as_view(), name='invoice_update'),
    url(r'^invoices/(?P<pk>\d+)/delete/$', InvoiceDelete.as_view(), name='invoice_delete'),
    url(r'^api/', include(router.urls, namespace='api')),
    url(r'^$', TemplateView.as_view(template_name='landing.html'), name='landing'),
]


if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.views import serve as static_serve
    staticpatterns = static(settings.STATIC_URL, view=static_serve)
    mediapatterns = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns = staticpatterns + mediapatterns + urlpatterns
