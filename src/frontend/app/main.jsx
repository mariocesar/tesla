require('bootstrap');
require('font-awesome/css/font-awesome.css');

import "./styles/main.scss"
import ReactDOM from "react-dom";
import Invoice from "./components/Invoice";

if (location.pathname.match(/^\/invoices\//)) {
    const invoiceEl = document.getElementById('invoice-form');

    if (!!invoiceEl) {
        const invoiceID = invoiceEl.getAttribute('data-id');
        ReactDOM.render(<Invoice invoiceID={invoiceID} />, invoiceEl);
    }
}
