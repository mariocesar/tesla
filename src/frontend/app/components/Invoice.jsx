import {Component} from "react";
import axios from 'axios';


class Input extends Component {
    constructor() {
        super()
        this.state = {edit: false}
    }

    handleDblClick(event) {
        this.setState({'edit': true})
    }

    handleOnBlur(event) {
        this.setState({'edit': false})
        this.props.handleSave(this.props.name, this.refs.input.value)
    }

    render() {
        let content;
        if (this.state.edit) {
            content = <input
                ref="input"
                name={this.props.name}
                type="text"
                defaultValue={this.props.defaultValue}
                className="form-control"
                onBlur={this.handleOnBlur.bind(this)} />
        } else {
            const editMsg = <span>
                <span className="fa fa-pencil" /> 
                Editar
            </span>;

            content = <span onDoubleClick={this.handleDblClick.bind(this)} className="editable">
                {this.props.defaultValue !== '' ? this.props.defaultValue : editMsg}
            </span>
        }

        return <div className="form-group">
            {content}
        </div>

    }
}


class Invoice extends Component {
    constructor(props) {
        super(props)
        this.state = {
            invoice: {
                id: null,
                ref: "",
                customer_name: "",
                entries: []
            }
       }
    }

    componentWillMount() {
        if (this.props.invoiceID !== "") {
            axios.get(`/api/invoices/${this.props.invoiceID}/`).then((response)=>{
                this.setState({invoice: this.props.invoice})
            })
        }
    }

    render() {
        const create = this.state.invoice.id === null;

        return <div className="paper">
            <h2>Invoice {this.state.invoice.ref}</h2>

            <div className="row">
                <div className="col-sm">
                    <p>
                    <strong>
                    </strong>
                    </p>
                    <p>
                    </p>
                </div>
                <div className="col-sm">
                    <label htmlFor="customer_name">Nombre del Cliente</label>
                    <Input defaultValue={this.state.invoice.customer_name} name="customer_name" />
                </div>
            </div>
            <hr />
            <table className="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Total</th>
                        <th />
                    </tr>
                </thead>
                <tbody>
                    {this.state.invoice.entries.map(function(entry, key) {
                        return <tr key={key}>
                            <td>{entry.description}</td>
                            <td>{entry.quantity}</td>
                            <td>{entry.price}</td>
                            <td>{entry.quantity * entry.price}</td>
                            <td>
                               <span className="fa fa-trash-o" />
                            </td>
                        </tr>
                    })}
                   </tbody>
            </table>
            <button className="btn btn-primary">
            {create ? "Crear" : "Guardar"}
            </button>
         </div>
    }
}

export default Invoice;
